var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), // mongo db connection
    bodyParser = require('body-parser'), // parses info from post
    methodOverride = require('method-override'); // used to manipulate post data


    router.use(bodyParser.urlencoded({extended: true}));
    router.use(methodOverride(function(req, res){
        if(req.body && typeof req.body == 'object' && '_method' in req.body){
            // look in urlencoded POST bodies and delete it
            var method = req.body._method;
            delete req.body._method;
            return method;
        }
    }));

    var courseAndStudentsModel = mongoose.model('CourseWithStudents');
    var courseModel = mongoose.model('Course');
    var studentModel = mongoose.model('Student');
    // Build the REST operatioins at the base of students
    // This will be accessible from localhost:9999/students 
    router.route('/')
        // GET all 
        .get(function(req, res, next){
            //  retrieve all  from the db
            courseModel.find({}, function(err, course){
                if(err){
                    return  console.error(err);
                } else {
                    res.format({
                        json: function(){
                            res.json(course);
                        }
                    });
                }
            });
        })
        // Post a new course for upgrading to a new term. 
        .post(function(req, res){
            courseModel.create({
                Name: req.body.Name,
                Description: req.body.Description,
                Instructor: req.body.Instructor                
            }, function(err, data) {
                if (err){
                    res.send("There was a problem adding new course to the db");
                } else {
                    res.format({
                        json: function(){
                            res.json(data);
                        }
                    });
                }
            });
        });

     // route middleware to validata :id
    router.param('courseName', function(req, res, next, courseName){
        courseModel.findOne({'Name': courseName}, function(err, data){
            if(err || data === null){
                res.status(404);
                err = new Error('Not Found');
                err.status = 404;
                res.format({
                    // html: function(){
                    //     next(err);
                    // },
                    json: function(){
                        res.json({message: err.status + ' ' + err});
                    }
                });
            } else{
                // once validation is done, save new id in the req
                req.courseName = courseName;
                next();
            }
        });
    });
    router.param('taUserName', function(req, res, next, taUserName){
        studentModel.findOne({'Username': taUserName}, function(err, data){
            if(err || data === null){
                res.status(404);
                err = new Error('Not Found');
                err.status = 404;
                res.format({
                    // html: function(){
                    //     next(err);
                    // },
                    json: function(){
                        res.json({message: err.status + ' ' + err});
                    }
                });
            } else{
                // once validation is done, save new id in the req
                req.taUserName = taUserName;
                next();
            }
        });
    });
    router.route('/lists/:courseName')
        .get(function(req, res)
        {
            courseAndStudentsModel.findOne({'Name': req.courseName}, function(err, data)
            {
                if(err)
                {
                    res.status(404);
                    err = new Error('GET error, problem retrieving data');
                    err.status = 404;
                    res.format({
                        json: function() 
                        {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                    //Data we got was good. 
                    studentModel.find({'Courses': req.courseName}, function(err, studentsFound)
                    {
                        if(err)
                        {

                            res.format({
                                json: function()
                                {
                                    res.json(data);
                                }
                            });
                        } else {
                            
                            data.students = [];
                            var pushNamesOntoData = function(student){
                                // var element = {'Name':student.Name, 'Username':student.Username};
                                // data.Students.push(element);
                                data.Students.push(student);
                            };
                            studentsFound.forEach(pushNamesOntoData);
                            res.format({
                                json: function()
                                {
                                    res.json(data);
                                }
                            });
                        }
                    });

                }
            });
        });
    router.route('/:courseName/:taUserName')
        .delete(function(req, res){
            courseModel.findOne({'Name':req.courseName}, function(err, data)
            {
                if(err)
                {
                    res.status(404);
                    err = new Error('GET error, problem finding data');
                    err.status = 404;
                    res.format({
                        json: function() 
                        {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                            
                    var tempTAs = [];
                    var generateNewList = function(student){
                        if (student.Username !== req.taUserName)
                        {
                        // var element = {'Name':student.Name, 'Username':student.Username};
                        // tempTAs.push(element);
                          tempTAs.push(student);
                        }
                    };
                    data.Needs.CurrentTAs.forEach(generateNewList);
                    data.Needs.CurrentTAs = tempTAs;

                    data.save(function(err, obj){
                        if(err){
                            res.status(404);
                            err = new Error('Problem updating student');
                            err.status = 404;
                            res.format({
                                json: function() {
                                    res.json({message: err.status + ' ' + err});
                                }
                            });
                        } else {
                            res.status(204);
                            res.format({
                                json: function(){
                                    res.json(null);
                                }
                            });
                        }
                    }); 
                }
            });

        });
    router.route('/:courseName')
        .get(function(req, res)
        {
            courseAndStudentsModel.findOne({'Name': req.courseName}, function(err, data)
            {
                if(err)
                {
                    res.status(404);
                    err = new Error('GET error, problem retrieving data');
                    err.status = 404;
                    res.format({
                        json: function() 
                        {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {                  

                    res.format({
                        json: function()
                        {
                            res.json(data);
                        }
                    });

                }
            });
        })//// courseAndStudentsModel.findOne({'Name': req.courseName}, function(err, course){
        .put(function(req, res){
            courseModel.findOne({'Name': req.courseName}, function(err, course){
                if(err){
                    res.status(404);
                    err = new Error('Coursename does not exist');
                    err.status = 404;
                    res.format({
                        json: function() {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                    var data = req.body;                    
                    if (typeof course.Needs === 'undefined'){
                        //course.Needs = data.Needs;
                    }else if (typeof data.Needs !== 'undefined') {
                        
                        if(typeof data.Needs.CurrentTAs !== 'undefined'){
                            if (course.Needs.CurrentTAs.length !== 0){
                                var temp = [];

                                var putOnCourseNeeds = function(element){
                                    if(element.Username !== 'undefined'){                                    
                                        temp.push(element);
                                    }
                                };
                                data.Needs.CurrentTAs.forEach(putOnCourseNeeds);
                                course.Needs.CurrentTAs.forEach(putOnCourseNeeds);
                                course.Needs.CurrentTAs = temp;                                
                            } else {
                                course.Needs.CurrentTAs = data.Needs.CurrentTAs;
                            }
                            
                        }
                        course.Needs.Graders = data.Needs.Graders;
                            course.Needs.ClassAssistants = data.Needs.ClassAssistants;
                            course.Needs.LabAssistants = data.Needs.LabAssistants;
                        
                        // if(typeof data.Needs.LabAssistants !== 'undefined'){
                        //     course.Needs.LabAssistants =  data.Needs.LabAssistants;
                        // }
                        // if(typeof data.Needs.ClassAssistants !== 'undefined'){
                        //     course.Needs.ClassAssistants =  data.Needs.ClassAssistants;
                        // }
                        // if(typeof data.Needs.Graders !== 'undefined'){
                        //     course.Needs.Graders =  data.Needs.Graders;
                        // }
                        
                        
                    }
                    course.save(function(err, obj){
                        if(err){
                            res.status(404);
                            err = new Error('Problem updating student');
                            err.status = 404;
                            res.format({
                                json: function() {
                                    res.json({message: err.status + ' ' + err});
                                }
                            });
                        } else {
                            res.format({
                                json: function(){
                                    res.json(obj);
                                }
                            });
                        }
                    });                    
                }

            });
        })
        //Shouldn't really ever have to do this during normal use. 
        //Should be used for removing students in future quarters. 
        .delete(function(req, res){
            courseModel.findOneAndRemove({'Name':req.courseName})
            .exec(
                function(err, data){
                    if(err){
                        res.status(404);
                        err = new Error('Problem deleting course. Why would you do that?');
                        err.status = 404;
                        res.format({
                            json: function(){
                                res.json({message : err.status + " " + err});
                            }
                        });
                    } else {
                        res.status(204);
                        console.log(data);
                        res.format({
                            json: function(){
                                res.json(null);
                            }
                        });
                    }

                }
            );
        });

        router.route('/:courseName/getStudents')
        // GET all 
        .get(function(req, res, next)
        {
            studentModel.find({'Courses' : req.courseName}, function(err, students)
            {
                if(err)
                {
                    return  console.error(err);
                } 
                else 
                {
                    res.format({
                        json: function()
                        {
                            res.json(students);
                        }
                    });
                }
            });
        });




module.exports = router;
