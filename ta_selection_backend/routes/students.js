var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), // mongo db connection
    bodyParser = require('body-parser'), // parses info from post
    methodOverride = require('method-override'); // used to manipulate post data


    router.use(bodyParser.urlencoded({extended: true}));
    router.use(methodOverride(function(req, res){
        if(req.body && typeof req.body == 'object' && '_method' in req.body){
            // look in urlencoded POST bodies and delete it
            var method = req.body._method;
            delete req.body._method;
            return method;
        }
    }));
    var studentModel = mongoose.model('Student');
    // Build the REST operatioins at the base of students
    // This will be accessible from localhost:9999/students 
    router.route('/')
        // GET all 
        .get(function(req, res, next){
            //  retrieve all  from the db
            studentModel.find({}, function(err, students){
                if(err){
                    return  console.error(err);
                } else {
                    res.format({
                        json: function(){
                            res.json(students);
                        }
                    });
                }
            });
        })
        // Post a new student. 
        // I don't think we should ever do this except for testing -
        // and upgrading to a new term. 
        .post(function(req, res){
            studentModel.create({
                Username: req.body.Username,
                Name: req.body.Name,
                GraduationDate: req.body.GraduationDate,
                hasWorkStudy: req.body.hasWorkStudy, 
                Courses: req.body.hasWorkStudy,
                AppliedCourses: req.body.AppliedCourses
                
            }, function(err, student) {
                if (err){
                    res.send("There was a problem adding student to the db");
                } else {
                    res.format({
                        json: function(){
                            res.json(student);
                        }
                    });
                }
            });
        });

     // route middleware to validata :id
    router.param('un', function(req, res, next, un){
        studentModel.findOne({Username: un}, function(err, student){
            if(err || student === null){
                res.status(404);
                err = new Error('Not Found');
                err.status = 404;
                res.format({
                    // html: function(){
                    //     next(err);
                    // },
                    json: function(){
                        res.json({message: err.status + ' ' + err});
                    }
                });
            } else{
                // once validation is done, save new id in the req
                req.un = un;
                next();
            }
        });
    });

    router.route('/:un')
        .get(function(req, res){
            studentModel.findOne({'Username': req.un}, function(err, student){
                if(err){
                    res.status(404);
                    err = new Error('GET error, problem retrieving data');
                    err.status = 404;
                    res.format({
                        json: function() {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                    res.format({
                        json: function(){
                            res.json(student);
                        }
                    });
                }
            });
        })
        .put(function(req, res){
            studentModel.findOne({'Username': req.un}, function(err, student){
                if(err){
                    res.status(404);
                    err = new Error('Username does not exist');
                    err.status = 404;
                    res.format({
                        json: function() {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                    var data = req.body;
                    
                    if (typeof data.Courses !== 'undefined') {
                        student.Courses = student.Courses.concat(data.Courses);
                    }
                    if (typeof data.hasWorkStudy !== 'undefined'){
                        student.hasWorkStudy = data.hasWorkStudy;
                    }
                    if (typeof data.AppliedCourses !== 'undefined'){
                        student.AppliedCourses = data.AppliedCourses;
                    }
// Put additional czechs here for <future> application form. ################################################

                    student.save(function(err, person){
                        if(err){
                            res.status(404);
                            err = new Error('Problem updating student');
                            err.status = 404;
                            res.format({
                                json: function() {
                                    res.json({message: err.status + ' ' + err});
                                }
                            });
                        } else {
                            res.format({
                                json: function(){
                                    res.json(person);
                                }
                            });
                        }
                    });                    
                }

            });
        })
        //Shouldn't really ever have to do this during normal use. 
        //Should be used for removing students in future quarters. 
        .delete(function(req, res){
            studentModel.findOneAndRemove({'Username':req.un})
            .exec(
                function(err, student){
                    if(err){
                        res.status(404);
                        err = new Error('Problem deleting student');
                        err.status = 404;
                        res.format({
                            json: function(){
                                res.json({message : err.status + " " + err});
                            }
                        });
                    } else {
                        res.status(204);
                        console.log(student);
                        res.format({
                            json: function(){
                                res.json(null);
                            }
                        });
                    }

                }
            );
        })
        ;

module.exports = router;
