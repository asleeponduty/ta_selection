var mongoose = require('mongoose');
// Why are things capitalized? That is because our seed files were,
// and all their fields are. So we shall do the same. 
var studentSchema = new mongoose.Schema({    
    Username: String,
    Name: String,
    GraduationDate: Date,
    Majors: [String],
    hasWorkStudy: {type: Boolean , default: false}, 
    Courses: [String],
    AppliedCourses: [{Name: String, Accepted: {type: Boolean, default: false}}]
    //Please add additional fields here for student app form. 
});
//mongoose.model(name, [schema], [collection])
mongoose.model('Student', studentSchema, 'students');


var individualTASchema = new mongoose.Schema({
    taStudent : studentSchema,
    taType: {type: String}
},{ _id : false });

// var needsSchema = new mongoose.Schema({
//     Needs: {
//         Graders: {type: Number, default: 0},
//         ClassAssistants: {type: Number, default: 0},
//         LabAssistants: {type: Number, default: 0},
//         CurrentTAs: {type : [individualTASchema],default:[]}
//     }
// });
var needsSchema = new mongoose.Schema({
    Graders: {type: Number, default: 0},
    ClassAssistants: {type: Number, default: 0},
    LabAssistants: {type: Number, default: 0},
    CurrentTAs: {type : [individualTASchema],default:[]}
});
var courseSchema = new mongoose.Schema({
    Name: String,
    Description: String,
    Instructor: String,
    Notes: {type: String, default: ""},
    Needs : {Graders: {type: Number, default: 0},
             ClassAssistants: {type: Number, default: 0},
             LabAssistants: {type: Number, default: 0},
             CurrentTAs: {type : [individualTASchema],default:[]}}
    
});
// seSchema.pre("save",function(next){
//     if(this.Needs)
// });
mongoose.model('Course', courseSchema, 'courses');

// var courseWithStudentsSchema = new mongoose.Schema({
//     Name: String,
//     Description: String,
//     Instructor: String,
//     Students: [{Name: String, Username: String}]
// });
var courseWithStudentsSchema = new mongoose.Schema({
    Name: String,
    Description: String,
    Instructor: String,
    Students: [studentSchema],
    Needs : {
        Graders: {type: Number, default: 0},
        ClassAssistants: {type: Number, default: 0},
        LabAssistants: {type: Number, default: 0},
        CurrentTAs: {type : [individualTASchema],default:[]}
    }
    
});
mongoose.model('CourseWithStudents', courseWithStudentsSchema, 'courses');

/*
    //Unknown if faculty are needed in the db at this time
    //The database has been seeded despite this. 
var facultySchema = new mongoose.Schema({
    Username: String,
    Name: String
});
mongoose.model('Faculty', facultySchema, 'faculty');
*/
