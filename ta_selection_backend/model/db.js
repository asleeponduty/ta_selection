var mongoose = require('mongoose');

var gracefulShutdown;
var dbUri = 'mongodb://localhost/ta_selection';
mongoose.connect(dbUri);

// Emulateing disconnection events on Windows
var readLine = require('readline');
if(process.platform === "win32"){
    var rl = readLine.createInterface({
        input : process.stdin,
        output: process.stdout
    });

    rl.on('SIGINT', function(){
        process.emit('SIGINT');
    });
}

// CONNECTION EVENTS
// Monirtoring for successful connection through Mongoose
mongoose.connection.on('connected', function(){
    console.log('Mongoose connected to ' + dbUri);
});

// Checking for connection error
mongoose.connection.on('error', function(err){
    console.log('Mongoose connection error ' + err);
});

// Checking for disconnection event
mongoose.connection.on('disconnected', function(){
    console.log('Mongoose is disconnected');
});

// CAPTURE APP TERMINATION / RESTART EVENTS
gracefulShutdown = function(msg, callback){
    mongoose.connection.close(function(){
        console.log("Mongoose disconnection through " + msg);
        callback();
    });
};

// FOr app termination
process.on('SIGINT', function(){
    gracefulShutdown("app termination", function(){
        process.exit(0);
    });
});

// Listens for SIGUSR2, whih is hat nodemon uses when it restarts app
process.once('SIGUSR2', function(){
    gracefulShutdown('nodemon reatart', function(){
        process.kill(process.id, 'SIGUSR2');
    });
});

// For Heroku app termination
process.on('SIGTERM', function(){
    gracefulShutdown('Heroku app termination', function(){
        process.exit(0);
    });
});


require('./schemas.js');
