# CSSE:490-01 TA Selection Project #
  
## Setup ##

- Start MongoDb with `mongod`. 
- Execute `setup.sh` located in `mongoSetup` to load the required data into the database
- Run `npm install` in both of the following directories: 
    - `ta_selection_frontend`
    - `ta_selection_backend`



## Running ##

- Start MongoDb with `mongod` in the terminal
- Navigate to the back-end folder `/ta_selection-backend` and start it using `npm start`
- Navigate to the front-end folder `/ta_selection-frontend` and start it using `npm start`