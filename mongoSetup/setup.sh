mongo ta_selection --eval "db.dropDatabase()"
mongoimport --db ta_selection --collection faculty --type json --file Faculty_201630.dat.json --jsonArray
mongoimport --db ta_selection --collection students --file ta_selection_students.json
mongoimport --db ta_selection --collection courses --type json --file Course_201630.dat.json --jsonArray
