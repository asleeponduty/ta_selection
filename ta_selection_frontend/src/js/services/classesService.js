app.service('classesService', ['$http','$log' ,'Course',function ($http,$log,Course) {
    var self = this;
    
    self.classes = [];
    self.CSSEclasses = [];
    self.init = function(){
        Course.query(function(data){
            self.classes=data;
        });
    };
    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     * Found from mozilla developer network
     */
    self.getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    self.updateCSSEClasses = function(courses){
        courses = [courses[0],courses[1]];
        courses.forEach(function(course){
            var courseGet = Course.get({id : course._id},function(data){
                console.log('Here is data');
                console.log(data);
                if (typeof course.Needs !== 'undefined') {
                    if(typeof course.Needs.LabAssistants !== 'undefined'){
                        data.Needs.LabAssistants =  course.Needs.LabAssistants;
                    }
                    if(typeof course.Needs.ClassAssistants !== 'undefined'){
                        data.Needs.ClassAssistants =  course.Needs.ClassAssistants;
                    }
                    if(typeof course.Needs.Graders !== 'undefined'){
                        data.Needs.Graders =  course.Needs.Graders;
                    }
                    
                }
                if(typeof course.Notes !== 'undefined'){
                    data.Notes =  course.Notes;
                }
                console.log(data);
                Course.update({id : course._id},data);
            });
        });
    };
    self.getCSSEClasses = function(callback){
      if(self.CSSEclasses.length !== 0){
          console.log('Csse classes length wasnt zero so we use that');
          callback(self.CSSEclasses);
      }
      else if(self.classes.length === 0 ){
          console.log('Classes length was zero so we must query');
          Course.query(function(data){
              self.classes=data;
              self.classes.forEach(function(classObj){
                  if(classObj.Name.substring(0,4) == "CSSE"){
                      //this is the case where classObj is a CSSE class
                      classObj.sizemin = self.getRandomInt(0,24);
                      classObj.sizemax = self.getRandomInt(24,28);
                      self.CSSEclasses.push(classObj);
                  }
              });
              callback(self.CSSEclasses);
          });;
      }else{
          console.log('Classes already exists. Just build csseClasses');
          self.classes.forEach(function(classObj){
              if(classObj.Name.substring(0,4) == "CSSE"){
                  //this is the case where classObj is a CSSE class
                  classObj.sizemin = self.getRandomInt(0,24);
                  classObj.sizemax = self.getRandomInt(24,28);
                  classObj.Notes = "";
                  self.CSSEclasses.push(classObj);
              }
          });
          callback(self.CSSEclasses);
      }

      
    };
    self.getNextClasses = function(){
        console.log(self.classes);
        if(typeof self.classes == 'undefined'){
            self.init();
        }
        return self.classes;
    };
    self.getClassFromClassName = function(className,callback){

        if(self.classes.length !== 0){
            self.classes.forEach(function(classObj,ind,arr){
                if(classObj.Name == className){
                    //if className matches the class name of one of the classes
                    // in our collection then return that object in the callback
                    callback(classObj);
                    return;
                }
            });
        }else{
            Course.get(className,function(classObj){
                callback(classObj);
            });
        }
        
    };


}]);
