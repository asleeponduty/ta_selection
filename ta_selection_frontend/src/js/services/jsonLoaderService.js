app.service('jsonLoaderService', function($http, $log)
{
    var self = this;
    self.request = function(URI, callback){
        // $log.log('Logging data');
        self.classes = [];
        
        //support for requesting more than one data file. 
        var pullData = function(elementURI, index, array)
        {
            $http({
                url: elementURI,
                method: 'GET',
                transformResponse: [function (data) {
                    data = rj.parse(data);
                    return data;
                }]
            }).success (function (data) {
               
                self.classes = data; //Todo: make data append when more than one class term retrieved. Priority: very low. 
                
                //This callback can be called multiple times. 
                callback(self.classes);
            });

        };
        URI.forEach(pullData);
        
    };
});