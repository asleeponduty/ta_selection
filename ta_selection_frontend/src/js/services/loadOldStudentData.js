app.service('loadOldStudentData', ['$http','$log','jsonLoaderService' ,function ($http,$log,jsonLoaderService) {
    /*
    This service, when run, will use the backend to import old student data 
    into a pre-seeded Mongo database. 
     */
    var self = this;
    self.run = function(callback){
        var dataFiles = [
            'json/Student_201410.dat.json',
            'json/Student_201420.dat.json',
            'json/Student_201430.dat.json',
            'json/Student_201440.dat.json',
            'json/Student_201510.dat.json',
            'json/Student_201520.dat.json',
            'json/Student_201530.dat.json',
            'json/Student_201540.dat.json',
            'json/Student_201610.dat.json'
            ];
        var loadIntoDB = function(element, index, array){
            var username = element.Username;
            var req = {
                 method: 'PUT',
                 url: 'http://localhost:9999/students/'+username,
                 data: { 'Courses': element.Courses }
                };
            $http(req);
        };
        jsonLoaderService.request(dataFiles, function(singleResponseData)
        {
            singleResponseData.forEach(loadIntoDB);
        });
    };

}]);