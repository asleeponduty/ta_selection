app.service('csvParse', function ($http,$log) {
    var self = this;

    this.title = "";
        //Example-info: {'Term':"Winter 2015-2016",'Instructor':"mutchler",'Credits':4,'Number of Students':29,
        //  'Capacity':24, 'Term Schedule':"TRF/2-3/O269", 'Finals Schedule':"M at 6pm in O267"};
    this.info = {};
    this.data = {}; 
    this.isProcessComplete = false;


    this.stripData = function(original_csv, callback){
        self.isProcessComplete = false;
        $http.get(original_csv).success(function(data){
            //Split this data up by line, read first 3 lines
            var splitData = data.split('\n');
            self.title = splitData[0];
            var courseInfoLine = splitData[1];
            var termInfoLine = splitData[2];
            //Parse out three lines and digest the data
            self.parseInfoLines(courseInfoLine);
            self.parseInfoLines(termInfoLine);
            
            //The data that is ready for BabyParse to digest for us
            var cleanData = splitData.slice(3);
            var cleanCSV = cleanData.join('\n');
            self.data = Baby.parse(cleanCSV, {header:true}).data;
            $log.log(self.data);
            $log.log(self.info);
            //self.parseCourseInfo();
            callback();
        });
    };
    this.parseInfoLines = function(infoString){
        // Data is split by TWO spaces! *whew*
        //Example-0: Term=Winter 2015-2016  Instructor=mutchler  Credits=4  Number of Students=29  Capacity=24
        //Example-1: Term Schedule=TRF/2-3/O269  Finals Schedule=M at 6pm in O267
        
        var splitData = infoString.split('  ');
        var arrayLength = splitData.length;
        for (var i = 0; i < arrayLength; i++) {
            var dequoted = splitData[i].replace('"','');
            var dataPairs = dequoted.split('=');
            self.info[dataPairs[0]]=dataPairs[1];
    
        }
    };


});
