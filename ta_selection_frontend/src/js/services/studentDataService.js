app.service('studentDataService', ['$http','$log' ,'courseService','Student', function ($http,$log,courseService,Student) {

    //This is the master service in the webapp
    var self = this;
    
    //self.some_class is set by TACollectionCtrl (origin: ng-repeat some_class in classes) and used by StudentDataCtrl
        
    self.students = [];
    self.csseStudents = [];
    self.init = function(){
        Student.query(function(data){
           self.students=data;
        });
    };
    self.studentsInClass = [];
    self.getStudentsFromMostRecentClass = function(){
        //Eventually we will have a data model of all students, and then all students in the upcoming class.
        //TODO: When our data model is updated, this should be changed to only include students from the upcoming quarter
        if(self.students.length === 0){
            self.init();
        }
        return self.students;
    };
    self.getStudentFromStudentUsername = function(studentUsername,callback){
        //Callback takes the student as a parameter
        if(typeof self.detailStudent != 'undefined'){
            callback(self.detailStudent);
        }else{
            Student.get({id:studentUsername},function(student){
                callback(student);
                return;
            });
        };
        
    };
    self._csseClassesFilter = function(){
            self.students.forEach(function(student){
                for(var i = 0; student.Courses.length > i ; i++){
                    var studentclass = student.Courses[i];
                    if(self.isCSSEClass(studentclass)){
                        //$log.log('Push student: ' + student.Username);
                        self.csseStudents.push(student);
                        break;
                    };
                };
            });
        };
    self.isCSSEClass = function(className){
        var isCSSE = className.substring(0,4) === 'CSSE';
        return(isCSSE);
    };
    self.getStudentsWithCSSECourses = function(callback){
        if(self.csseStudents.length !== 0){
            console.log('Skip generating if we already have data');
            callback(self.csseStudents);
        }else if(self.students.length !== 0){
            console.log('Students already loaded, find csseClasses');
            courseService.getCSSECourses(function(csseClasses){
                self._csseClassesFilter(csseClasses);
                callback(self.csseStudents);
            });
        }else{
            console.log('Students not loaded, query then find csseClasses');
            Student.query(function(data){
                self.students=data;
                courseService.getCSSECourses(function(csseClasses){
                    self._csseClassesFilter(csseClasses);
                });
                callback(self.csseStudents);
            });
        }
    };
    self.getStudentsFromClassName = function(className){
        //trim last 3 characters since we want things like IA451-01 to make other IA451 classes
        //This name should be passed by id in the url.K
        courseService.getCourseAndStudents(className, function(data){
            self.studentsInClass = data.Students;
        });
        
        return studentsInClass;
    };

}]);
