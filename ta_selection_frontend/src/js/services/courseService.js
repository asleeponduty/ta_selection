app.service('courseService', ['$http','$log' ,'CourseList', 'CourseAndStudents','Course',function ($http,$log,CourseList, CourseAndStudents,Course) {
    var self = this;
    
    //self.class = {};
    self.classes = [];
    self.CSSEclasses = [];

    self.init = function(){
      self.getCourses();
    };

    self.getCourses = function(callback){
        CourseList.query(function(data){
            self.classes = data;
            if (typeof callback !== 'undefined'){
              callback(data);
            }
        });
        
    };
    self.getCSSECourses = function(callback){
        if (self.CSSEclasses.length !== 0){
          callback(self.CSSEclasses);
          return;
        } 
        if (self.classes.length !== 0){
            self.classes.forEach(function(classObj){

                  if(classObj.Name.substring(0,4) == "CSSE"){
                      //this is the case where classObj is a CSSE class
                      classObj.sizemin = self.getRandomInt(0,24); //Fix this. 
                      classObj.sizemax = 24;
                      self.CSSEclasses.push(classObj);
                  }
              });
            callback(self.CSSEclasses);
          return;
        }

        CourseList.query(function(data){
          data.forEach(function(classObj){
                  if((classObj.Name.substring(0,4) == "CSSE")||(classObj.Name.substring(0,2) == "MA")){
                      //this is the case where classObj is a CSSE class
                      classObj.sizemin = self.getRandomInt(0,24); //Fix this. 
                      classObj.sizemax = 24;
                      self.CSSEclasses.push(classObj);
                  }
              });
            callback(self.CSSEclasses);
        });
    };
    self.getCourseAndStudents = function(courseName, callback){
        CourseAndStudents.getSingle({'name':courseName },function(data){
            callback(data);
        });
    };
    self.updateCSSEClass = function(course){
        $log.log('course being updated');
        $log.log(course);
        CourseAndStudents.getSingle({'name' : course.Name},function(data){
            $log.log(data);
            data.Needs = course.Needs;
            $log.log('Entered needs undefined');
            console.log(course.Name);
            CourseAndStudents.update({'name' : course.Name},data);
        });
    };
    self.updateCSSEClasses = function(courses){
        courses.forEach(function(element){
                //console.log(data);
                Course.update({'name' : element.Name},element);
        });
    };

    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     * Found from mozilla developer network
     */
    self.getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

}]);
