app.controller('StudentWSCtrl', ['$scope','$location','$log','studentDataService', function ($scope,$location, $log,studentDataService) {
    
    $scope.isLoading = true;
    $scope.displayedData = [];

    studentDataService.getStudentsWithCSSECourses(function(data){
        $scope.isLoading = false;
        $scope.studentData = data;
        $scope.studentData.forEach(function(student){
            if(student.AppliedCourses.length == 0){
                student.hasApplied = false;
            }else{
                for(var i = 0; i < student.AppliedCourses.length ; i++) {
                    var courseApplied = student.AppliedCourses[i];
                    if(courseApplied.Accepted){
                        student.hasApplied = true;
                    };
                };
                student.hasApplied = false;
            }
        });
    });
    $scope.getters = {
        workStudy : function(value){
            return value.hasWorkStudy? 1 : 0 ;
        },
        name : function(value){
            var splitted = value.Name.split(' '); 
            return splitted[splitted.length-1];
        },
        applied : function(student){
            if(student.AppliedCourses.length == 0){
                $log.log('false');
                return false;
            }else{
                for(var i = 0; i < student.AppliedCourses.length ; i++) {
                    var courseApplied = student.AppliedCourses[i];
                    if(courseApplied.Accepted){
                        $log.log('true');
                        return true;
                    };
                };
                $log.log('false');
                return false;
            }
        }
    };
    $scope.goToDetail = function(student){
        var baseUrl = '/student-detail/';
        window.scrollTo(0,0);
        $location.url(baseUrl + student.Username + '/workstudy');
        studentDataService.detailStudent = student;
    };
    
}]);
