app.controller('StudentDetailCtrl',['$log','$scope','studentDataService','$location','$routeParams', function($log,$scope,studentDataService,$location,$routeParams){
    $scope.studentUserName = $routeParams.studentUsername;
    $scope.previousClassName = $routeParams.className;
    
    $scope.isLoading = true;
    var setDisplayStudent = function(student){
        $log.log(student);
        $scope.detailStudent = student;
        $scope.displayStudent = {'Name':$scope.detailStudent.Name,
                                 'Username':$scope.detailStudent.Username,
                                 'Year' : $scope.detailStudent.Year,
                                 'Major' : $scope.detailStudent.Majors[0]
                                };
        $scope.displayCourses = $scope.detailStudent.Courses;
        $scope.isLoading = false;
    };
    studentDataService.getStudentFromStudentUsername($scope.studentUserName,setDisplayStudent);

    $scope.goToListStudents = function(viewLoc){
        var baseUrl = '/course-view/';
        //studentDataService.fromWorkStudy = true;
        window.scrollTo(0,0);
        $location.url(baseUrl + viewLoc );
    };
}]);
