app.controller('TACollectionCtrl', ['$scope', '$location','studentDataService','courseService','$log', function ($scope, $location, studentDataService,courseService,$log) {
    var scope = $scope;

    scope.classes = [];
    scope.displayClasses = [];
    $scope.isLoading = true;
    courseService.getCSSECourses(function(data){
        $scope.isLoading = false;
        scope.classes = data;
        console.log(data);
    });
    $scope.goTo = function(args){
        var viewLoc = args.viewLoc;
        
        var baseUrl = '/course-view/';
        window.scrollTo(0,0);
        $location.url(baseUrl + viewLoc );
    };
}]);
