app.controller('TANeedsCtrl', ['$scope', '$location','$log','courseService', function ($scope, $location,$log,courseService) {
    
    
    $scope.classes = [];
    $scope.displayClasses = [];
    $scope.isLoading = true;
    courseService.getCSSECourses(function(data){
        $scope.isLoading  = false;
        $scope.classes = data;
        $log.log($scope.classes);

    });
    
    $scope.saveForm = function(){
        $scope.maintaform.$setPristine();
        courseService.updateCSSEClasses($scope.classes);
        

    };
}]);
