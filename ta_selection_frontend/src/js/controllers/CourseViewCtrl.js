app.controller('CourseViewCtrl', ['$scope','$location','$log','courseService','$routeParams','$uibModal', function ($scope,$location, $log, courseService, $routeParams,$uibModal) {
    
    //$scope.className is set by the TAcontroller and the ClassesController in their respective goto functions. 

    $scope.className = $routeParams.className;
    $scope.isLoading = true;
    $scope.Needs = {};
    courseService.getCourseAndStudents($scope.className, function(data)
    {
        $scope.data = data;
        $log.log('data');
        $log.log(data);
        $scope.data = data;
        $scope.studentData = data.Students;
        $scope.Needs = data.Needs;
        //build our list of which students are ta.
        $scope.updateLists($scope.data,$scope.Needs);
        //$scope.className = data.className;
        $scope.displayedData = [].concat($scope.studentData);
        $scope.display_class = {'Description':data.Description,
                                'Instructor':data.Instructor,
                                'CRN':data.CRN};
        $scope.isLoading = false;
    });
    $scope.updateLists = function(studentData,Needs){
        //data is the set we are trimming
        if(typeof Needs !== 'undefined'){
            if(Needs.CurrentTAs.length != 0){
                Needs.CurrentTAs.forEach(function(taObj){
                    //taObj has taStudent and taType field
                    var indexToRemove = studentData.indexOf(taObj.taStudent);
                    if(indexToRemove > -1){
                        $scope.studentData.splice(indexToRemove,1);
                    }
                });
            }else{
                $log.log('No current TAs.');
            }
        }else{
            //else initalize
            $log.log('Needs is undefined');
            
        }
    };
    $scope.hideTAs = false;
    $scope.HideTaCondition = function(){
        return (typeof Needs === 'undefined') || (typeof Needs.CurrentTAs === 'undefined') ||  (Needs.CurrentTAs.lenth > 0);
    };
    $scope.getSpecificTAs = function(Needs, taString){
        var TAS = [];
        if(typeof Needs === 'undefined' ){
            return;
        }
        Needs.CurrentTAs.forEach(function(taObj){
            if(taObj.taType == taString){
                TAS.push(taObj);
            }
        });
    };
    $scope.addTA = function(student,taTypeIn){
        $scope.RecentlyPressedStudent = student;
        $scope.open();
    };
    $scope.removeTA = function(student){
        //Modify so student is remove from grading list
        //$log.log(student);
        for(var i = 0; i<$scope.Needs.CurrentTAs.length ; i++){
            var taObj = $scope.Needs.CurrentTAs[i];
            $log.log('Needs user: ' + taObj.taStudent.Username + '  studentData :  ' + student.Username);
            if(taObj.taStudent.Username === student.Username){
                $log.log('Low splice');
                var deletedTA = $scope.Needs.CurrentTAs.splice(i,1);
                $log.log(deletedTA);
                $scope.studentData.push(deletedTA);
                break;
            }
        }
        //TODO: Now delete from backend
        //this part was not implemented by the time sprint 3 was over
        
    };
    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'views/templates/modalDisplay.html',
            controller: 'ModalInstanceManager'
        });

        modalInstance.result.then(function (result) {
            var taSchema = {taStudent : $scope.RecentlyPressedStudent , taType  : result};
            $scope.data.Needs.CurrentTAs.push(taSchema);
            $scope.updateLists($scope.studentData,$scope.Needs);
            courseService.updateCSSEClass($scope.data);
            
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.goToDetail = function(student){
        var baseUrl = '/student-detail/';
        window.scrollTo(0,0);
        $location.url(baseUrl + student.Username + '/' + $scope.className);
        
    };
}]);

