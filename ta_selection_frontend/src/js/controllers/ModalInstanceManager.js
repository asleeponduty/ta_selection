app.controller('ModalInstanceManager', function ($scope,$uibModalInstance,$log) {
    $scope.selectedButton = 1;
    var getNameFromNumber = function(num){
        if(num == 1){
            return 'Graders';
        }else if(num == 2){
            return 'ClassAssistants';
        }else {
            return 'LabAssistants';
        }
    };
    $scope.ok = function () {
        $uibModalInstance.close(getNameFromNumber($scope.selectedButton));
        
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.press = function(num){
        $scope.selectedButton = num;
        $log.log($scope.selectedButton);
    };
});
