//app.run(['studentDataService','classesService','loadOldStudentData',  function (studentDataService,classesService,loadOldStudentData) {
app.run(['studentDataService','courseService',  function (studentDataService,courseService) {
    //Initialize session data for service.
    
    // var nextClassesDataFile = ['src/json/Course_201630.json'];
    // classesService.init(nextClassesDataFile);
    courseService.init();
    
    // var studentsDataFiles = ['src/json/Student_201620.json'];
    // studentDataService.init(studentsDataFiles);
    studentDataService.init();

    //Run this once to add old student data to your MongoDb
    //loadOldStudentData.run();
}]);
