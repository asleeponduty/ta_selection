(function(){
    var app = angular.module('DataManager', []);

    app.factory('EnvConfig', ['environment', function(environment){
        var staging = {
            api: {
                baseUrl: 'http://localhost:9999/',
                suffix: ''
            }
        };
        var prod = {
            api: {
                baseUrl: 'http://localhost:9999/',
                suffix: ''
            }
        };
        var configs = {staging: staging, prod: prod};
        return configs[environment];
    }]);

app.factory('Student', ['$resource', 'EnvConfig',
    function($resource, EnvConfig) {
        var url = EnvConfig.api.baseUrl + 'students/:id' + EnvConfig.api.suffix;
        return $resource(url, {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.factory('CourseList', ['$resource', 'EnvConfig',
    function($resource, EnvConfig) {
        var url = EnvConfig.api.baseUrl + 'courses' + EnvConfig.api.suffix;
        return $resource(url, {});
    }
]);
app.factory('Course', ['$resource', 'EnvConfig', function($resource, EnvConfig) {
        var url = EnvConfig.api.baseUrl + 'courses/:name';
        // var posturl = EnvConfig.api.baseUrl + 'coursespost/:id' + EnvConfig.api.suffix;
        return $resource(url, 
            {},
            {   
                update: {method: 'PUT', params: {name: '@name'}},
                getSingle: {method: 'GET', params: {name: '@name'}}
            }

            );
}]);
app.factory('CourseAndStudents', ['$resource', 'EnvConfig', function($resource, EnvConfig) {
        var url = EnvConfig.api.baseUrl + 'courses/lists/:name';
        // var posturl = EnvConfig.api.baseUrl + 'coursespost/:id' + EnvConfig.api.suffix;
        return $resource(url, 
            {},
            {   
                update: {method: 'PUT', params: {name: '@name'}},
                getSingle: {method: 'GET', params: {name: '@name'}}
            }

            );
}]);

})();
