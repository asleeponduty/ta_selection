app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
        })
        .when('/ta-collection', {
            templateUrl: 'views/ta-collection.html',
            controller: 'TACollectionCtrl'
        })
        .when('/course-view/:className', {
            templateUrl: 'views/course-view.html',
            controller: 'CourseViewCtrl'
        })        
        .when('/student-detail/:studentUsername/:className',{
            templateUrl: 'views/student-detail.html',
            controller: 'StudentDetailCtrl'
        })
        .when('/student-workstudy',{
            templateUrl: 'views/student-workstudy.html',
            controller: 'StudentWSCtrl',
            css: 'css/student-workstudy.css'
        })
        .when('/ta-needs',{
            templateUrl: 'views/ta-needs.html',
            controller: 'TANeedsCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
    //use HTML5 history API
    $locationProvider.html5Mode(true);
});
